<?php
/*
	Plugin Name: WP Lead Plus X
	Plugin URI: http://www.wpleadplus.com/
	Description: Create your responsive landing pages, squeeze pages, lead pages, popup widgets within minutes. Working well on mobile, desktop, tablets
	Author: core37, codingpuss
	Version: 1.10.12
	Author URI: http://www.core37.com/
	Text Domain: wp-lead-plus-x
*/
include_once 'functions.php';
