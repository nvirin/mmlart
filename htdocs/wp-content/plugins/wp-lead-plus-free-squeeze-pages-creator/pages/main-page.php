<div id="c37-lp-settings-page">
	<div id="tabs">
		<ul id="tab-pills">
			<li><a href="#main">Main</a></li>
			<li><a href="#templates">Templates</a> </li>
		</ul>


		<div id="main">
			<h1>Thanks for using WP Lead Plus X</h1>
			<p>If you have any question, please send a message here: <a target="_blank" href="http://core37.com/contact">contact us</a></p>
			<p>You can also find a list of tutorials of WP Lead Plus X <a target="_blank" href="https://www.youtube.com/playlist?list=PLlMUKQq5jx5_cSsO8Q_koMnYPWImyPlya">here</a></p>
			<p>This is a totally new version of WP Lead Plus. If you want to use the old version (which is no longer supported), please download it <a href="http://core37.com/file/wp-lead-plus-free-squeeze-pages-creator.zip">here</a> and ignore all future updates.</p>
			<p>You can also find the instructions to re-install the old version <a href="https://youtu.be/5N-8epI90M0">here</a></p>
		</div>

		<div id="templates">
			<p>Pre-designed templates are available in the PRO version only. <a href="http://core37.com/wp-lead-plus-x-pro/">Please upgrade to use this feature</a></p>
		</div>


<!--			PRO version	-->
<!--		<div id="main">-->
<!--			<h1>Thanks for purchasing WP Lead Plus X</h1>-->
<!--			<p>If you have any question, please send a message here: <a target="_blank" href="http://core37.com/contact">contact us</a></p>-->
<!--			<p>You can also find a list of tutorials of WP Lead Plus X <a target="_blank" href="https://www.youtube.com/playlist?list=PLlMUKQq5jx5_cSsO8Q_koMnYPWImyPlya">here</a></p>-->
<!--		</div>-->
<!---->
<!--		<div id="templates">-->
<!--			<h3>Load templates</h3>-->
<!--			<p>Please click the "Load templates" button to load all the templates come with this plugin</p>-->
<!--			<button id="load-templates">Load templates</button>-->
<!---->
<!--		</div>-->

	</div>




</div>
